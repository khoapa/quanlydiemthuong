<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Model\Criteria;
use DateTime;
use App\Model\ManageScoress;

class ManageScores extends Controller
{
    public function index(Request $request)
    {
        if (!empty($request->date)) {
            $date = $request->date;
            $arrayDate = explode("-", $date);
            $months =  $arrayDate['0'];
            $year = $arrayDate['1'];
        } else {
            $date = date("m-Y");
            $months =  date("m");
            $year = date("Y");
        }
        $staff = User::listManageScores($request->name, $months, $year, '5');
        return view('admins.manageScore.index', compact('staff', 'date'));
    }
    public function create($id)
    {
        $staff = User::findOrFail($id);
        $criteria1 = Criteria::where('status', '1')->get();
        $criteria2 = Criteria::where('status', '2')->get();
        return view('admins.manageScore.create', compact('staff', 'criteria1', 'criteria2'));
    }
    public function store(Request $request, $id)
    {
        $arrayCriteria = $request->criteria;
        if(!is_null($request->date)){
            $a = new DateTime($request->date);
            $date = $a->format('Y-m-d');
        }else{
            $date = date("Y-m-d");
        }
        try {
            foreach ($arrayCriteria as $value) {
                $data = [];
                $data = [
                    'criteria_id' => $value,
                    'user_id' => $id,
                    'date' => $date
                ];
                ManageScoress::create($data);
            }
            return redirect()->route('manage.index')->with('message',  'Thêm mới Điểm thành công');
        } catch (\Exception $e) {
            return redirect()->back()->with('message', 'Đã có lỗi xảy ra trong quá trình cập nhật. Vui lòng thử lại ');
        }
    }
}
