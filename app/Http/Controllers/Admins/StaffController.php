<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Model\Positions;
use DateTime;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\SaffRequest;
use Illuminate\Support\Facades\File;
use App\Http\Requests\SaffUpdateRequest;

class StaffController extends Controller
{

    public function index(Request $request)
    {
        if (!empty($request->name) or !empty($request->positions_id )) {
            $staff = User::searchStaff($request->name, $request->positions_id, '5');
        } else {
            $staff = User::listStaff('ASC', '5');
        }
        $positions = Positions::get();
        return view('admins.staff.index', compact('staff','positions'));
    }

    public function create()
    {
        $positions = Positions::get();
        return view('admins.staff.create', compact('positions'));
    }
    public function store(SaffRequest $request)
    {
        $data = $request->all();
        $birthDay = new DateTime($request->birthday);
        $data['birthday'] = $birthDay->format('Y-m-d');
        $data['password'] = Hash::make($request->password);
        $file = $request->avatar;
        if (!empty($file)) {
            $tailImage = $file->getClientOriginalExtension();
            $data['avatar'] = strtotime(date('Y-m-d H:i:s')) . '.' . $tailImage;
            $path = public_path('upload/staff/');
        }
        try {
            if (User::create($data)) {
                if (!empty($file)) {
                    $file->move($path, $data['avatar']);
                }
            }
            return redirect()->route('staff.index')->with('message',  'Thêm mới "Nhân Viên" thành công');
        } catch (\Exception $e) {
            return redirect()->back()->with('message', 'Đã có lỗi xảy ra trong quá trình cập nhật. Vui lòng thử lại ');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    public function edit($id)
    {
        $staff = User::findOrFail($id);
        $positions = Positions::get();
        return view('admins.staff.update', compact('staff','positions'));
    }
    public function update(SaffUpdateRequest $request, $id)
    {
        $staff = User::findOrFail($id);
        $data = $request->all();
        $birthDay = new DateTime($request->birthday);
        $data['birthday'] = $birthDay->format('Y-m-d');
        if ($request->password != null) {
            $data['password'] = Hash::make($request->password);
        } else {
            $data['password'] = $staff->password;
        }
        $message        = 'Đã cập nhật thông tin "Nhân Viên" thành công.';
        $errorUpdate   = 'Đã có lỗi xảy ra trong quá trình cập nhật. Vui lòng thử lại.';
        if ($request->has('avatar')) {
            $file = $request->avatar; 
            $avatarCurrent = public_path('upload/staff/' . $staff->avatar); 
            $tailImage = $file->getClientOriginalExtension();
            $data['avatar'] = strtotime(date('Y-m-d H:i:s')) . '.' . $tailImage; 
            $path = public_path('upload/staff/'); 
        } else {
            $data['avatar'] = $staff->avatar;
        }
        try {
            if ($staff->update($data)) {
                if ($request->has('avatar')) {
                    $file->move($path, $data['avatar']);
                    if (File::exists($avatarCurrent)) {
                        File::delete($avatarCurrent);
                    }
                }
            }
            return redirect()->route('staff.index')->with('message', $message);
        } catch (\Exception $e) {
            return redirect()->back()->with('message', $errorUpdate);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $message        = 'Đã xóa "Nhân viên" thành công.';
        $error_delete   = 'Đã có lỗi xảy ra trong quá trình xóa. Vui lòng thử lại.';
        $staff = User::findOrFail($id);
        $avatar = public_path('upload/staff/' . $staff->avatar);
        try {
            if ($staff->delete()) {
                if (File::exists($avatar)) {
                    File::delete($avatar);
                }
            }
            return redirect()->route('staff.index')->with('message', $message);
        } catch (\Exception $e) {
            return redirect()->back()->with('message', $error_delete);
        }
    }
}
