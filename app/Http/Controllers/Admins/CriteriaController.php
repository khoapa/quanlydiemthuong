<?php

namespace App\Http\Controllers\Admins;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Criteria;
use App\Http\Requests\CriteriaRequest;
use App\Model\ManageScoress;

class CriteriaController extends Controller
{
    public function index(Request $request)
    {
        if (!empty($request->name) or !empty($request->positions_id )) {
            $criteria = Criteria::searchStaff($request->name, $request->positions_id, '5');
        } else {
            $criteria = Criteria::listCriteria('ASC', '5');
        }
        return view('admins.criteria.index', compact('criteria'));
    }
    public function create()
    {
        return view('admins.criteria.create');
    }
    public function store(CriteriaRequest $request)
    {
        $data = $request->all();
        try {
            Criteria::create($data);
            return redirect()->route('criteria.index')->with('message',  'Thêm mới "Tiêu Chí" thành công');
        } catch (\Exception $e) {
            return redirect()->back()->with('message', 'Đã có lỗi xảy ra trong quá trình cập nhật. Vui lòng thử lại ');
        }
    }
    public function edit($id)
    {
        $criteria = Criteria::findOrFail($id);
        return view('admins.criteria.update', compact('criteria'));
    }
    public function update(CriteriaRequest $request, $id)
    {
        $message        = 'Đã cập nhật thông tin "Tiêu Chí" thành công.';
        $errorUpdate   = 'Đã có lỗi xảy ra trong quá trình cập nhật. Vui lòng thử lại.';
        $criteria = Criteria::findOrFail($id);
        $data = $request->all();
        try {
            $criteria->update($data);
            return redirect()->route('criteria.index')->with('message', $message);
        } catch (\Exception $e) {
            return redirect()->back()->with('message', $errorUpdate);
        }
    }
    public function destroy($id)
    {
        $message        = 'Đã xóa "Tiêu Chí" thành công.';
        $error_delete   = 'Đã có lỗi xảy ra trong quá trình xóa. Vui lòng thử lại.';
        $staff = Criteria::findOrFail($id);
        try {
            if ($staff->delete()) {
                $criteria1 = ManageScoress::where('criteria_id', $id)->delete();
            }
            return redirect()->route('staff.index')->with('message', $message);
        } catch (\Exception $e) {
            return redirect()->back()->with('message', $error_delete);
        }
    }
}
