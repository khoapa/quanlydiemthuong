<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ScoresController extends Controller
{
    public function index(Request $request)
    {
        if (!empty($request->date)) {
            $date = $request->date;
            $arrayDate = explode("-", $date);
            $months =  $arrayDate['0'];
            $year = $arrayDate['1'];
        } else {
            $date = date("m-Y");
            $months =  date("m");
            $year = date("Y");
        }
        $staff = User::listManageScores($request->name, $months, $year, '5');
        return view('frontend.scores.index', compact('staff', 'date'));
    }
    public function show(Request $request)
    {
        if (!empty($request->date)) {
            $date = $request->date;
            $arrayDate = explode("-", $date);
            $months =  $arrayDate['0'];
            $year = $arrayDate['1'];
        } else {
            $date = date("m-Y");
            $months =  date("m");
            $year = date("Y");
        }
        $user = User::find(Auth::id())->with(['criteria' => function ($q) use ($months, $year) {
            $q->whereMonth('date', '=', $months);
            $q->whereYear('date', '=', $year);
        }])->where([
            ['level', '=', '2'],
        ])->firstOrFail();
        return view('frontend.scores.show', compact('date','user'));
    }
}
