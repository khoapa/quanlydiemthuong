<?php

namespace App\Http\Controllers\Frontend;
//
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class MemberController extends Controller
{
    public function showLogin()
    {
        return view('frontend.login.index');

    }
    public function login(Request $request)
    {
        $login = [
            'email' => $request->email,
            'password' => $request->pass,
        ];
        $remember = false;

        if ($request->remember) {
            $remember = true;
        }
        if ($this->doLogin($login, $remember)) {
            return redirect()->route('scores.index');
        } else {
            return redirect()->back()->withErrors('Email or password is not correct.');
        }   
    }
    protected function doLogin($attempt, $remember)
    {
        if (Auth::attempt($attempt, $remember)) {
            return true;
        } else {
            return false;
        }
    }
    public function logout(Request $request)
    {   
        Auth::logout();
        return redirect()->route('frontend.login.showLogin');
    }
}
