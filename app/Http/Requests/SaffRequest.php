<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SaffRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  =>  'required|min:4',
            'email' => 'required|email|unique:users', 
            'password'   => 'required',
            'birthday'  =>  'required',
            'avatar'  =>  'required|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'phone'  =>  'required|min:9|max:12',
            'address'  =>  'required|min:10',
            'gender'  =>  'required|not-in:0',
        ];
    }
    public function messages()
    {
        return [
            'required'            =>  __(':attribute không được để trống.'),
            'unique'              =>  __(':attribute ' . '"' . $this->email . '"' . ' đã tồn tại trong hệ thống, vui lòng nhập :attribute khác.'),
            'min'            =>  __(':attribute tối thiểu 4 ký tự.'),
            'mimes'            =>  __('đây không phải file :attribute'),
            'max'            =>  __(':attribute quá dung lượng quy định.'),
            'not_in'            =>  __(':attribute Bạn chưa chọn :attribute.'),
        ];
    }
    public function attributes()
    {
        return [
            'name'     =>  __('Họ Tên'),
            'email'     =>  __('email'),
            'password'     =>  __('Mật Khẩu'),
            'birthday'     =>  __('Ngày Sinh'),
            'avatar'     =>  __('Hình'),
            'phone'     =>  __('Số Điện Thoại'),
            'address'     =>  __('Địa Chỉ'),
            'gender'     =>  __('Giới tính'),
        ];
    }
}
