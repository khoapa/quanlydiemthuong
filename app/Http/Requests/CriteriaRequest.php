<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CriteriaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  =>  'required|min:4',
            'scores' => 'required', 
            'status'   => 'required',
        ];
    }
    public function messages()
    {
        return [
            'required'            =>  __(':attribute không được để trống.'),
            'unique'              =>  __(':attribute ' . '"' . $this->email . '"' . ' đã tồn tại trong hệ thống, vui lòng nhập :attribute khác.'),
            'min'            =>  __(':attribute tối thiểu 4 ký tự.'),
        ];
    }
    public function attributes()
    {
        return [
            'name'     =>  __('Tên Tiêu Chí'),
            'scores'     =>  __('Điểm'),
            'status'     =>  __('Trạng Thái'),
        ];
    }
}
