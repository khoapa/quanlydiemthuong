<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Positions extends Model
{
    protected $table = 'positions';
    protected $fillable = ['name'];
    public $timestamps = true;
    public function users()
    {
        return $this->hasMany('App\User');
    }
}
