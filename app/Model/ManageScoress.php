<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ManageScoress extends Model
{
    protected $table = 'manage_scores';
    protected $fillable = ['user_id','criteria_id','date'];
    public $timestamps = true;
}
