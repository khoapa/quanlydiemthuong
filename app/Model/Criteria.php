<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Criteria extends Model
{
    protected $table = 'criteria';
    protected $fillable = ['name','scores','status'];
    public $timestamps = true;

    public static function listCriteria($Sort = 'DESC', $limit = 10)
    {
        if ($Sort == 'DESC') {
            return Criteria::orderBy('id', 'DESC')->paginate($limit);
        } else {
            return Criteria::orderBy('id', 'ASC')->paginate($limit);
        }
    }
    public function User()
    {
        return $this->belongsToMany('App\User','manage_scores');
    }
}
