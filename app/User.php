<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Model\Criteria;
use App\Model\ManageScoress;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'avatar', 'phone', 'birthday',
        'address', 'gender', 'positions_id', 'level',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function positions()
    {
        return $this->belongsTo('App\Model\Positions', 'positions_id');
    }
    public function criteria()
    {
        return $this->belongsToMany('App\Model\Criteria', 'App\Model\ManageScoress');
    }
    public static function listManageScores($name = null, $months = null, $year = null, $limit = 10)
    {
        if ($name == null) {
            return User::with(['criteria' => function ($q) use ($months, $year) {
                $q->whereMonth('date', '=', $months);
                $q->whereYear('date', '=', $year);
            }])->where('level', '2')->paginate($limit);
        } else {
            return User::with(['criteria' => function ($q) use ($months, $year) {
                $q->whereMonth('date', '=', $months);
                $q->whereYear('date', '=', $year);
            }])->where([
                ['level', '=', '2'],
                ['name', "like", '%' . mb_strtolower($name, 'UTF-8') . '%']
            ])->paginate($limit);
        }
    }
    public static function listStaff($Sort = 'DESC', $limit = 10)
    {
        if ($Sort == 'DESC') {
            return User::with('positions')->where('level', '=', '2')->orderBy('id', 'DESC')->paginate($limit);
        } else {
            return User::with('positions')->where('level', '=', '2')->orderBy('id', 'ASC')->paginate($limit);
        }
    }
    public static function searchStaff($name = null, $positions_id = null, $limit = 10)
    {
        if ($name == null) {
            return User::with('positions')->where([
                ['level', '=', '2'],
                ['positions_id', '=', $positions_id]
            ])->orderBy('id', 'DESC')->paginate($limit);
        } else if ($positions_id == 0) {
            return User::with('positions')->where([
                ['level', '=', '2'],
                ['name', "like", '%' . mb_strtolower($name, 'UTF-8') . '%']
            ])->orderBy('id', 'DESC')->paginate($limit);
        } else {
            return User::with('positions')->where([
                ['level', '=', '2'],
                ['positions_id', '=', $positions_id],
                ['name', "like", '%' . mb_strtolower($name, 'UTF-8') . '%']
            ])->orderBy('id', 'DESC')->paginate($limit);
        }
    }
}
