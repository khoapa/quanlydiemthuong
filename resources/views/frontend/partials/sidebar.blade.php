<div class="app-sidebar__inner">
    <ul class="vertical-nav-menu">
        <li class="app-sidebar__heading">Quản Lý Thông tin</li>
        <li>
            <a href="{{route('scores.index')}}">
                <i class="metismenu-icon pe-7s-diamond"></i>
                Danh Sách Điểm Nhân Viên
                <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
            </a>
        </li>
    </ul>
</div>