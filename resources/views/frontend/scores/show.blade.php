@extends('frontend')
@section('content')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-car icon-gradient bg-mean-fruit">
                    </i>
                </div>
                <div>Danh Sách Chi Tiết Điểm Từng Tháng</div>
            </div>
            <div class="page-title-actions">
                <button type="button" data-toggle="tooltip" title="Example Tooltip" data-placement="bottom" class="btn-shadow mr-3 btn btn-dark">
                    <i class="fa fa-star"></i>
                </button>
            </div>
        </div>
    </div>
    <div class="main-card mb-3 card">
        <div class="card-body">
            <div id="example_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <form action="{{route('scores.show')}}" method="get">
                    <label for="sel1">Search:</label>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <div>
                                    <input type="text" class="form-control" width="276" id="datepickerMonths" name="date" placeholder="{{$date}}">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Search</button>
                            </div>
                        </div>
                    </div>
                </form>

                <div class="row">
                    <div class="col-sm-12">
                        <table class="table">
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Tên lỗi</th>
                                    <th scope="col">Điểm</th>
                                    <th scope="col">Trạng thái</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $i = 0;
                                @endphp
                                @foreach($user->criteria as $valueCriteria)
                                <tr>
                                    <th scope="row">{{++$i}}</th>
                                    <td>{{$valueCriteria->name}}</td>
                                    <td>{{$valueCriteria->scores}}</td>
                                    <td>
                                        @if($valueCriteria->status == 1)
                                            Cộng
                                        @else
                                            trừ
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-5">
                        <div class="dataTables_info" id="example_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection