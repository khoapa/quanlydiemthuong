@extends('admin')
@section('content')
<div class="app-main__inner">
    <div class="app-page-title">
        <div class="page-title-wrapper">
            <div class="page-title-heading">
                <div class="page-title-icon">
                    <i class="pe-7s-car icon-gradient bg-mean-fruit">
                    </i>
                </div>
                <div>Danh Sách Nhân Viên</div>
            </div>
            <div class="page-title-actions">
                <button type="button" data-toggle="tooltip" title="Example Tooltip" data-placement="bottom" class="btn-shadow mr-3 btn btn-dark">
                    <i class="fa fa-star"></i>
                </button>
            </div>
        </div>
    </div>
    <div class="main-card mb-3 card">
        <div class="card-body">
            <a href="{{route('staff.create')}}" class="btn btn-primary stretched-link">Create</a>
        </div>
        <div class="card-body">
            <div id="example_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <form action="{{route('staff.index')}}" method="get">
                    <label for="sel1">Search:</label>
                    <div class="row">
                        <div class="col-md-3">
                            <input type="search" name="name" class="form-control form-control-sm" placeholder="Tên Nhân Viên" aria-controls="example">
                        </div>
                        <div class="col-md-3">
                            <select class="form-control" id="positions_id" name="positions_id">
                                <option value="0">Chức Vụ</option>
                                @foreach($positions as $valuePositions)
                                <option value="{{$valuePositions->id}}">{{$valuePositions->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Search</button>
                            </div>
                        </div>
                    </div>
                </form>

                <div class="row">
                    <div class="col-sm-12">
                        <table style="width: 100%;" id="example" class="table table-hover table-striped table-bordered dataTable dtr-inline" role="grid" aria-describedby="example_info">
                            <thead>
                                <tr role="row">
                                    <th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 149.2px;" aria-sort="ascending" aria-label="Name: activate to sort column descending">Họ Tên</th>
                                    <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 50px;" aria-label="Position: activate to sort column ascending">Ảnh</th>
                                    <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 105.2px;" aria-label="Office: activate to sort column ascending">Ngày Sinh</th>
                                    <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 54.2px;" aria-label="Age: activate to sort column ascending">Chức Vụ</th>
                                    <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 109.2px;" aria-label="Start date: activate to sort column ascending">Email</th>
                                    <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" style="width: 82.2px;" aria-label="Salary: activate to sort column ascending">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($staff as $valueStaff)
                                @php
                                $date=date_create($valueStaff->birthday);
                                @endphp
                                <tr role="row" class="even">
                                    <td class="sorting_1 dtr-control">{{$valueStaff->name}}</td>
                                    <td><img src="{{ asset('upload/staff') }}/{{$valueStaff->avatar}}" class="img-thumbnail" alt="Cinque Terre" width="50px" height="236"> </td>
                                    <td>{{ date_format($date," d-m-Y")}}</td>
                                    <td>{{$valueStaff->positions->name}}</td>
                                    <td>{{$valueStaff->email}}</td>
                                    <td>
                                        <div class="dropdown">
                                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                                Action
                                            </button>
                                            <div class="dropdown-menu">
                                                <a href="{{route('staff.edit',['id'=>$valueStaff->id])}}" class=" dropdown-item btn btn-primary stretched-link">Update</a>
                                                <a href="{{route('staff.destroy',['id'=>$valueStaff->id])}}" onclick="return confirm('Bạn có chắc chắn muốn xóa?')" class=" dropdown-item btn btn-primary stretched-link">delete</a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th rowspan="1" colspan="1">Họ Tên</th>
                                    <th rowspan="1" colspan="1">Ảnh</th>
                                    <th rowspan="1" colspan="1">Ngày Sinh</th>
                                    <th rowspan="1" colspan="1">Chức vụ</th>
                                    <th rowspan="1" colspan="1">Email</th>
                                    <th rowspan="1" colspan="1">Action</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div class="pagination-area">
                    <ul class="pagination">
                        {{ $staff->links() }}
                    </ul>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-5">
                        <div class="dataTables_info" id="example_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection