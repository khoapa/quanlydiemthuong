@extends('admin')
@section('content')
<div class="app-main__inner">
    <form action="{{route('staff.store')}}" enctype="multipart/form-data" method="post">
        @csrf
        <div class="main-card mb-3 card">
            <div class="card-body">
                <h5 class="card-title">Thêm mới nhân viên</h5>
                <form id="signupForm" class="col-md-10 mx-auto" method="post" action="" novalidate="novalidate">
                    <div class="form-group">
                        <label for="firstname">Họ Tên</label>
                        <div>
                            <input type="text" class="form-control" id="name" name="name" placeholder="Nguyễn Văn A">
                        </div>
                        @if( $errors->has('name') )
                        <div class="alert alert-danger">{{ $errors->first('name') }}</div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="birthday">Ngày Sinh</label>
                        <div>
                            <input type="text" class="form-control" width="276" id="datepicker" name="birthday" placeholder="25-10-2004">
                        </div>
                        @if( $errors->has('birthday') )
                        <div class="alert alert-danger">{{ $errors->first('birthday') }}</div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="sel1">Giới Tính:</label>
                        <select class="form-control" id="gender" name="gender">
                            <option value="0">Giớ Tính</option>
                            <option value="1">Nam</option>
                            <option value="2">Nữ</option>
                        </select>
                        @if( $errors->has('gender') )
                        <div class="alert alert-danger">{{ $errors->first('gender') }}</div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <div>
                            <input type="text" class="form-control" id="email" name="email" placeholder="Email">
                        </div>
                        @if( $errors->has('email') )
                        <div class="alert alert-danger">{{ $errors->first('email') }}</div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                        @if( $errors->has('password') )
                        <div class="alert alert-danger">{{ $errors->first('password') }}</div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Avatar</label>
                        <div class="row">
                            <div class="col-md-4">
                                <div>
                                    <span class="input-group-text">Upload hình</span>
                                    <input type="file" onchange=file_changed() id=input name="avatar" class="form-control hidden">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <img id=img width="100px">
                            </div>
                        </div>
                        @if( $errors->has('avatar') )
                        <div class="alert alert-danger">{{ $errors->first('avatar') }}</div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="phone">Số Điện thoại</label>
                        <div>
                            <input type="text" class="form-control" id="phone" name="phone" placeholder="09795000000">
                        </div>
                        @if( $errors->has('phone') )
                        <div class="alert alert-danger">{{ $errors->first('phone') }}</div>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="address">Địa Chỉ</label>
                        <div>
                            <input type="text" class="form-control" id="address" name="address" placeholder="Địa Chỉ">
                        </div>
                        @if( $errors->has('address') )
                        <div class="alert alert-danger">{{ $errors->first('address') }}</div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="sel1">Chức Vụ:</label>
                        <select class="form-control" id="positions_id" name="positions_id">
                            <option>Chức Vụ</option>
                            @foreach($positions as $valuePositions)
                            <option value="{{$valuePositions->id}}">{{$valuePositions->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="sel1">Quyền truy cập:</label>
                        <select class="form-control" id="level" name="level">
                            <option value="2">user</option>
                            <option value="1">admin</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Thêm mới</button>
                    </div>
                </form>
            </div>
        </div>
    </form>
</div>
@endsection