@extends('admin')
@section('content')
<div class="app-main__inner">
    <form action="{{route('criteria.store')}}" method="post">
        @csrf
        <div class="main-card mb-3 card">
            <div class="card-body">
                <h5 class="card-title">Thêm Mới Tiêu Chí</h5>
                <div class="form-group">
                    <label for="firstname">Tên Tiêu Chí</label>
                    <div>
                        <input type="text" class="form-control" id="name" name="name" placeholder="Đi làm muộn">
                    </div>
                    @if( $errors->has('name') )
                    <div class="alert alert-danger">{{ $errors->first('name') }}</div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="birthday">Số Điểm</label>
                    <div>
                        <input type="number" class="form-control" name="scores" placeholder="7">
                    </div>
                    @if( $errors->has('scores') )
                    <div class="alert alert-danger">{{ $errors->first('scores') }}</div>
                    @endif
                </div>
                <div class="form-group">
                    <input type="radio" name="status" value="1">
                    <label>Cộng Điểm</label><br>
                    <input type="radio" name="status" value="2">
                    <label for="female">Trừ Điểm</label><br>
                    @if( $errors->has('status') )
                    <div class="alert alert-danger">{{ $errors->first('status') }}</div>
                    @endif
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Thêm mới</button>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection