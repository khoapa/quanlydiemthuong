@extends('admin')
@section('content')
<div class="app-main__inner">
    <form action="{{route('criteria.update',['id'=>$criteria->id])}}" method="post">
        @csrf
        <div class="main-card mb-3 card">
            <div class="card-body">
                <h5 class="card-title">Cập Nhật Tiêu Chí</h5>
                <div class="form-group">
                    <label for="firstname">Tên Tiêu Chí</label>
                    <div>
                        <input type="text" class="form-control" value="{{$criteria->name}}" id="name" name="name" placeholder="Đi làm muộn">
                    </div>
                    @if( $errors->has('name') )
                    <div class="alert alert-danger">{{ $errors->first('name') }}</div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="birthday">Số Điểm</label>
                    <div>
                        <input type="number" value="{{$criteria->scores}}" class="form-control" name="scores" placeholder="7">
                    </div>
                    @if( $errors->has('scores') )
                    <div class="alert alert-danger">{{ $errors->first('scores') }}</div>
                    @endif
                </div>
                <div class="form-group">
                    @if($criteria->status == 1)
                    <input type="radio" checked name="status" value="1">
                    <label>Cộng Điểm</label><br>
                    <input type="radio" name="status" value="2">
                    <label for="female">Trừ Điểm</label><br>
                    @else
                    <input type="radio" name="status" value="1">
                    <label>Cộng Điểm</label><br>
                    <input type="radio" checked name="status" value="2">
                    <label for="female">Trừ Điểm</label><br>
                    @endif
                    @if( $errors->has('status') )
                    <div class="alert alert-danger">{{ $errors->first('status') }}</div>
                    @endif
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Thêm mới</button>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection