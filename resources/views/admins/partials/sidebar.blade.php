<div class="app-sidebar__inner">
    <ul class="vertical-nav-menu">
        <li class="app-sidebar__heading">Quản Lý Thông tin</li>
        <li>
            <a href="#">
                <i class="metismenu-icon pe-7s-diamond"></i>
                Quản Lý Hệ Thống
                <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
            </a>
            <ul>
                <li>
                    <a href="{{route('staff.index')}}">
                        <i class="metismenu-icon"></i>
                        Quản Lý Nhân Viên
                    </a>
                </li>
                <li>
                    <a href="{{route('criteria.index')}}">
                        <i class="metismenu-icon">
                        </i>Quản Lý Tiêu Chí
                    </a>
                </li>
                <li>
                    <a href="{{route('manage.index')}}">
                        <i class="metismenu-icon">
                        </i>Cập Nhật Điểm Số
                    </a>
                </li>
            </ul>
        </li>
    </ul>
</div>