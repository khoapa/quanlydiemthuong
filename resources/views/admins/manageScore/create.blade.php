@extends('admin')
@section('content')
<div class="app-main__inner">
    <form action="{{route('manage.store',['id'=>$staff->id])}}" enctype="multipart/form-data" method="post">
        @csrf
        <div class="main-card mb-3 card">
            <div class="card-body">
                <h5 class="card-title">Quản Lý Điểm</h5>
                <div class="modal-footer"></div>
                <div class="container">
                    <div class="row justify-content-center">
                        <h3>Thông Tin Nhân Viên </h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="firstname">Họ Tên</label>
                            <div>
                                <input type="text" class="form-control" id="name" value="{{$staff->name ?? ''}}" name="name" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="firstname">Ngày Sinh</label>
                            @php
                            $date=date_create($staff->birthday);
                            @endphp
                            <div>
                                <input type="text" class="form-control" value="{{ date_format($date,' d-m-Y') ?? ''}}" id="birthday" name="birthday" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="firstname">Chức Vụ</label>
                            <div>
                                <input type="text" class="form-control" id="name" value="{{$staff->positions->name}}" name="name" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col md-6">
                        <div class="container">
                            <img src="{{ asset('upload/staff') }}/{{$staff->avatar}}" class="mx-auto d-block" alt="Cinque Terre" width="200" height="200">
                        </div>
                    </div>
                </div>
                <div class="modal-footer"></div>
                <div class="container">
                    <div class="row justify-content-center">
                        <h3>Chọn Điểm</h3>
                    </div>
                </div>
                <div class="form-group">
                    <label for="birthday">Ngày Phạm Lỗi</label>
                    @php
                        $today = date("d/m/Y");
                    @endphp
                    <div>
                        <input type="text" class="form-control" width="276" id="datepicker" name="date" placeholder="{{$today}}">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <h4>Điểm Thưởng</h4>
                        <div class="form-group">
                            @foreach($criteria1 as $value)
                            <div class="form-check">
                                <input id="closeButton" type="checkbox" name="criteria[]" value="{{$value->id}}" class="form-check-input">
                                <label class="form-check-label" for="closeButton">
                                    {{$value->name}} : Điểm {{$value->scores}}
                                </label>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="col md-6">
                        <h4>Điểm Phạt</h4>
                        <div class="form-group">
                            @foreach($criteria2 as $value)
                            <div class="form-check">
                                <input id="closeButton" type="checkbox" name="criteria[]" value="{{$value->id}}" class="form-check-input">
                                <label class="form-check-label" for="closeButton">
                                    {{$value->name}} : Điểm {{$value->scores}}
                                </label>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Thêm mới</button>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection