<?php
// Admin route 
Route::group(['prefix' => 'admin'], function () {
    Route::get('/', 'Admins\DashboardController@index')
         ->name('admins.dashboard.index');
});