<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();
// frontend
Route::get('/', 'Frontend\MemberController@showLogin')->name('frontend.login.showLogin');
Route::group([
    'prefix' => 'member',
    'namespace' => 'Frontend',
], function () {
    Route::get('login', 'MemberController@showLogin')->name('frontend.login.showLogin');
    Route::post('loginMember', 'MemberController@login')->name('frontend.checkLogin.member');
    Route::post('memberlogout', 'MemberController@logout')->name('frontend.memberlogout');
});

Route::group([
    'prefix' => 'member',
    'namespace' => 'Frontend',
    'middleware' => ['auth.member']
], function () {
    Route::get('scores-list', 'ScoresController@index')->name('scores.index');
    Route::get('scores-show', 'ScoresController@show')->name('scores.show');
});



Route::group([
    'prefix' => 'admin',
    'namespace' => 'Auth',
], function () {

    Route::get('/login', 'LoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'LoginController@login');
    Route::post('/logout', 'LoginController@logout')->name('admin.logout');
});

Route::group([
    'prefix' => 'admin',
    'namespace' => 'Admins',
    'middleware' => ['admin']
], function () {
    // staff
    Route::get('staff-list', 'StaffController@index')->name('staff.index');
    Route::get('staff-create', 'StaffController@create')->name('staff.create');
    Route::post('staff-store', 'StaffController@store')->name('staff.store');
    Route::get('staff-edit/{id}', 'StaffController@edit')->name('staff.edit');
    Route::post('staff-update/{id}', 'StaffController@update')->name('staff.update');
    Route::get('staff-destroy/{id}', 'StaffController@destroy')->name('staff.destroy');

    //criteria
    Route::get('criteria-list', 'CriteriaController@index')->name('criteria.index');
    Route::get('criteria-create', 'CriteriaController@create')->name('criteria.create');
    Route::post('criteria-store', 'CriteriaController@store')->name('criteria.store');
    Route::get('criteria-edit/{id}', 'CriteriaController@edit')->name('criteria.edit');
    Route::post('criteria-update/{id}', 'CriteriaController@update')->name('criteria.update');
    Route::get('criteria-destroy/{id}', 'CriteriaController@destroy')->name('criteria.destroy');

    // 

    Route::get('manage-scores-list', 'ManageScores@index')->name('manage.index');
    Route::get('manage-scores-create/{id}', 'ManageScores@create')->name('manage.create');
    Route::post('manage-scores-store/{id}', 'ManageScores@store')->name('manage.store');
});
