<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersDatabaseSeeder::class);
        $this->call(PositionsDatabaseSeeder::class);
        $this->call(CriteriaDatabaseSeeder::class);
        $this->call(ManageDatabaseSeeder::class);

    }
}
class UsersDatabaseSeeder extends Seeder
{
    public function run()
    {
        $faker = Faker\Factory::create();

        $limit = 20;
        DB::table('users')->insert([
            [
                'name' => "admin",
                'email' => "admin@mail.com",
                'avatar' => 'annh.jpg',
                'phone' => '0979539600',
                'password' => Hash::make(12345678),
                'birthday' => $faker->dateTimeBetween('1990-01-01', '2012-12-31')->format('Y/m/d'),
                'address' => $faker->address,
                'gender' => rand(1, 2),
                'positions_id' => rand(1, 4),
                'level' => '1',
            ],
        ]);
        for ($i = 0; $i < $limit; $i++) {
            DB::table('users')->insert([
                [
                    'name' => $faker->name,
                    'email' => $faker->unique()->email,
                    'avatar' => 'annh.jpg',
                    'phone' => '0979539600',
                    'password' => Hash::make(12345678),
                    'birthday' => $faker->dateTimeBetween('1990-01-01', '2012-12-31')->format('Y/m/d'),
                    'address' => $faker->address,
                    'gender' => rand(1, 2),
                    'positions_id' => rand(1, 4),
                    'level' => '2',
                ],
            ]);
        }
    }
}
class PositionsDatabaseSeeder extends Seeder
{
    public function run()
    {
        $faker = Faker\Factory::create();

        $limit = 4;
        for ($i = 0; $i < $limit; $i++) {
            DB::table('positions')->insert([
                [
                    'name' => $faker->name,
                    
                ],
            ]);
        }
    }
}
class CriteriaDatabaseSeeder extends Seeder
{
    public function run()
    {
        $faker = Faker\Factory::create();

        $limit = 4;
        for ($i = 0; $i < $limit; $i++) {
            DB::table('criteria')->insert([
                [
                    'name' => $faker->name,
                    'scores' => rand(1,10),
                    'status' => rand(1,2),
                ],
            ]);
        }
    }
}
class ManageDatabaseSeeder extends Seeder
{
    public function run()
    {
        $faker = Faker\Factory::create();

        $limit = 10;
        for ($i = 0; $i < $limit; $i++) {
            DB::table('manage_scores')->insert([
                [
                    'user_id' => rand(1,3),
                    'criteria_id' => rand(1,4),
                    'date' =>  $faker->dateTimeBetween('2020-10-01', '2020-10-31')->format('Y/m/d'),
                ],
            ]);
        }
    }
}
